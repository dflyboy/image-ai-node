const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node-gpu');

const Data = require('./data');

// How many examples the model should "see" before making a parameter update.
const BATCH_SIZE = 64;
// How many batches to train the model for.
const TRAIN_BATCHES = 10;
// Every TEST_ITERATION_FREQUENCY batches, test accuracy over TEST_BATCH_SIZE examples.
// Ideally, we'd compute accuracy over the whole test set, but for performance
// reasons we'll use a subset.
const TEST_BATCH_SIZE = 100;
const TEST_ITERATION_FREQUENCY = 5;
const LEARNING_RATE = 0.001;

const DROPOUT_RATE = 0.2;

function createModel() {
    const model = tf.sequential();

    model.add(tf.layers.conv2d({
        inputShape: [32,32,3],
        kernelSize: 3,
        filters: 64,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 64,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling'
    }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2,2],
        strides: [2,2]
    }));

    // for(let i=0; i<3; i++) {
    //     // model.add(tf.layers.conv2d({
    //     //     kernelSize: 3,
    //     //     filters: 128,
    //     //     strides: 1,
    //     //     activation: 'relu',
    //     //     kernelInitializer: 'VarianceScaling',
    //     //     padding: 'same'
    //     // }));
    
    //     model.add(tf.layers.conv2d({
    //         kernelSize: 3,
    //         filters: 128,
    //         strides: 1,
    //         activation: 'relu',
    //         kernelInitializer: 'VarianceScaling'
    //     }));
    
    //     model.add(tf.layers.maxPooling2d({
    //         poolSize: [2, 2],
    //         strides: [2,2]
    //     }));
    // }

    model.add(tf.layers.dropout({ rate: DROPOUT_RATE }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling'
    }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2, 2],
        strides: [2,2]
    }));

    model.add(tf.layers.dropout({ rate: DROPOUT_RATE }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling',
        padding: 'same'
    }));

    model.add(tf.layers.conv2d({
        kernelSize: 3,
        filters: 128,
        strides: 1,
        activation: 'relu',
        kernelInitializer: 'VarianceScaling'
    }));

    model.add(tf.layers.maxPooling2d({
        poolSize: [2, 2],
        strides: [2, 2]
    }));
    model.add(tf.layers.dropout({ rate: DROPOUT_RATE }));

    model.add(tf.layers.flatten());

    model.add(tf.layers.dense({
        units: 1024,
        kernelInitializer: 'VarianceScaling',
        activation: 'relu'
    }));

    model.add(tf.layers.dropout({ rate: DROPOUT_RATE }));

    model.add(tf.layers.dense({
        units: 10,
        kernelInitializer: 'VarianceScaling',
        activation: 'softmax'
    }));

    return model;
}

async function train(model) {
    const optimizer = tf.train.adam(LEARNING_RATE);

    model.compile({
        optimizer: optimizer,
        loss: 'categoricalCrossentropy',
        metrics: ['accuracy'],
    });

    let tData = [data.testData.xs, data.testData.labels];

    const history = await model.fit(
        data.trainData.xs,
        data.trainData.labels, {
            batchSize: BATCH_SIZE,
            tData,
            epochs: TRAIN_BATCHES
        });

    // for (let i = 0; i < TRAIN_BATCHES; i++) {
    //     const batch = data.nextTrainBatch(BATCH_SIZE);

    //     let testBatch;
    //     let validationData;
    //     // Every few batches test the accuracy of the mode.
    //     if (i % TEST_ITERATION_FREQUENCY === 0) {
    //         testBatch = data.nextTestBatch(TEST_BATCH_SIZE);
    //         validationData = [
    //             testBatch.xs, testBatch.labels
    //         ];
    //     }

    //     // The entire dataset doesn't fit into memory so we call fit repeatedly
    //     // with batches.
    //     const history = await model.fit(
    //         batch.xs,
    //         batch.labels, {
    //             batchSize: BATCH_SIZE,
    //             validationData,
    //             epochs: 1
    //         });

    //     const loss = history.history.loss[0];
    //     const accuracy = history.history.acc[0];

    //     console.log(`[${i}/${TRAIN_BATCHES}] LOSS=${loss}; ACC=${accuracy}`);
    // }
}

async function run() {
    var model;
    if(process.argv[2]) model = await tf.loadModel('file://' + process.argv[2]);
    else model = createModel();
    await train(model);
    model.save('file://./cifar.model');
}

const data = new Data();
data.load().then(() => {
    console.log('data parsed');
    run();
    // const model = createModel();
    // train(model).then(() => {
    //     model.save('file://./cifar.model');
    // });
});