const tf = require('@tensorflow/tfjs');
const fs = require('fs');

const NUM_CLASSES = 10;
const IMAGE_W = 32, IMAGE_H = 32;
const IMAGE_PIXELS = IMAGE_H * IMAGE_W;
const IMAGE_SIZE = IMAGE_PIXELS * 3;
const DATA_SIZE = IMAGE_SIZE + 1;

class Data {
    constructor() {
        this.trainOff = 0;
        this.testOff = 0;
    }

    async load() {
        let res = await Data.readData('data/cifar/data_batch_5.bin');
        this.trainImages = res.img;
        this.trainLabels = res.labels;
        // res = await Data.readData('data/cifar/data_batch_3.bin');
        // this.trainImages = this.trainImages.concat(res.img);
        // this.trainLabels = this.trainLabels.concat(res.labels);

        this.trainCount = this.trainLabels.length;

        console.log('Generating training tensors');
        this.trainData = this.getTrainData();
        this.trainImages = null;
        this.trainLabels = null;

        res = await Data.readData('data/cifar/test_batch.bin');
        this.testImages = res.img;
        this.testLabels = res.labels;
        console.log('Generating test tensors');
        this.testData = this.getTestData();
        this.testImages = null;
        this.testLabels = null;
    }

    getTrainData() {
        const xs = tf.tensor4d(this.trainImages,  [this.trainImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 3]);
        const labels = tf.tensor2d(this.trainLabels);
        return {
            xs,
            labels
        };
    }

    /**
     * Get all test data as a data tensor a a labels tensor.
     *
     * @param {number} numExamples Optional number of examples to get. If not
     *     provided,
     *   all test examples will be returned.
     * @returns
     *   xs: The data tensor, of shape `[numTestExamples, 28, 28, 1]`.
     *   labels: The one-hot encoded labels tensor, of shape
     *     `[numTestExamples, 10]`.
     */
    getTestData() {
        let xs = tf.tensor4d(this.testImages, [this.testImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 3]);
        let labels = tf.tensor2d(this.testLabels);

        return {
            xs,
            labels
        };
    }

    nextTestBatch(num) {
        let offset = Math.floor(Math.random() * (this.testLabels.length - num));

        let xs = this.testData.xs.slice([offset, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 3]);
        let labels = this.testData.labels.slice([offset, 0], [num, NUM_CLASSES]);
        return {
            xs,
            labels
        };
    }

    nextTrainBatch(num) {
        let offset = Math.floor(Math.random() * (this.trainCount - num));

        let xs = this.trainData.xs.slice([offset, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 3]);
        let labels = this.trainData.labels.slice([offset, 0], [num, NUM_CLASSES]);
        return {
            xs,
            labels
        };
    }

    static readImages(file) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, (err, data) => {
                const num = data.readInt32BE(4);
                console.log(num + ' images in file');

                var offset = 16;

                var imgdata = data.slice(offset);
                var out = [];
                for(let i=0; i<imgdata.length; i++) {
                    out[i] = imgdata[i] / 255;
                }

                return resolve(out);
            });
        });
    }
    static readLabels(file) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, (err, data) => {
                const num = data.readInt32BE(4);
                console.log(num + ' labels in file');

                var labels = [];
                const offset = 8;

                for (let i = 0; i < num; i++) {
                    var label = [];
                    var res = data.readUInt8(offset + i);
                    for (let i = 0; i < NUM_CLASSES; i++) {
                        label[i] = res == i ? 1 : 0;
                    }
                    labels.push(label);
                    // labels[i] = data.readUInt8(offset + i);
                }

                return resolve(labels);
            });
        });
    }

    static readData(file) {
        return new Promise((resolve, reject) => {
            fs.readFile(file, (err, data) => {
                const num = data.byteLength / DATA_SIZE;
                console.log(num + ' images in file');

                // var imgdata = bops.subarray(data, offset);
                let labels = [];
                let img = [];
                for (let i = 0; i < num; i++) {
                    let label = [];
                    let res = data.readUInt8(i * DATA_SIZE);
                    for (let i = 0; i < NUM_CLASSES; i++) {
                        label[i] = res == i ? 1 : 0;
                    }
                    labels.push(label);

                    // let rows = [];
                    for (let j = 0; j < 1024; j++) {
                        for (let z = 0; z < 3; z++) {
                            // let channel = [];

                            let off = z * 1024 + 1 + i*DATA_SIZE;
                            let byte = data.readUInt8(j + off) / 255;
                            // rows[j][x][z] = byte;
                            // channel.push(byte);
                            img.push(byte);
                        }
                        // rows.push(channel);
                    }
                    // img = img.concat(rows.flat());
                }

                return resolve({
                    labels,
                    img
                });
            });
        });
    }
}

module.exports = Data;

// Data.readImages('mnist/train-images.idx3-ubyte').then(images => {
//     Data.readLabels('mnist/train-labels.idx1-ubyte').then(labels => {
//         const x = 1594;
//         let image = images[x];
//         let label = labels[x];
//         for (let i = 0; i < 28; i++) {
//             var s = '';
//             for (let j = 0; j < 28; j++) {
//                 s += image[i][j] > 0.3 ? 'X' : ' ';
//             }
//             console.console.log(s);
//         }
//         console.console.log( label);
//     });
// });