const NUM_CLASSES = 10;
const IMAGE_W = 28, IMAGE_H = 28;
const IMAGE_SIZE = IMAGE_H * IMAGE_W;

class Data {
    constructor() {
        this.trainOff = 0;
        this.testOff = 0;
    }

    load(cb) {
        $.getJSON('trainImg.json', trainImages => {
            this.trainImages = trainImages;
            $.getJSON('trainLabel.json', trainLabels => {
                this.trainLabels = trainLabels;
                $.getJSON('testImg.json', testImages => {
                    this.testImages = testImages;
                    $.getJSON('testLabel.json', testLabels => {
                        this.testLabels = testLabels;
                        cb();
                    })
                })
            })
        })
        // console.log(util.inspect(this.trainImages.slice(0,1)));
    }

    getTrainData() {
        const xs = tf.tensor4d(this.trainImages,  [this.trainImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 1]);
        const labels = tf.tensor2d(this.trainLabels);
        return {
            xs,
            labels
        };
    }

    /**
     * Get all test data as a data tensor a a labels tensor.
     *
     * @param {number} numExamples Optional number of examples to get. If not
     *     provided,
     *   all test examples will be returned.
     * @returns
     *   xs: The data tensor, of shape `[numTestExamples, 28, 28, 1]`.
     *   labels: The one-hot encoded labels tensor, of shape
     *     `[numTestExamples, 10]`.
     */
    getTestData() {
        let xs = tf.tensor4d(this.testImages, [this.testImages.length / IMAGE_SIZE, IMAGE_H, IMAGE_W, 1]);
        let labels = tf.tensor2d(this.testLabels);

        return {
            xs,
            labels
        };
    }

    nextTestBatch(num) {
        let data = this.getTestData();
        let xs = data.xs.slice([this.testOff, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 1]);
        let labels = data.labels.slice([this.testOff, 0], [num, NUM_CLASSES]);
        this.testOff = (this.testOff + num) % this.testLabels.length;
        return {
            xs,
            labels
        };
    }

    nextTrainBatch(num) {
        let data = this.getTrainData();
        let xs = data.xs.slice([this.trainOff, 0, 0, 0], [num, IMAGE_H, IMAGE_W, 1]);
        let labels = data.labels.slice([this.trainOff, 0], [num, NUM_CLASSES]);
        this.trainOff = (this.trainOff + num) % this.trainLabels.length;
        return {
            xs,
            labels
        };
    }
}